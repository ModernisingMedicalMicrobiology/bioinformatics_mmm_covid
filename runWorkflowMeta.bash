#!/bin/bash


data='/mnt/nanostore'
grid='grid0'
runName=$1
VOCs=$PWD/PHE_VOCs.csv
mkdir -p ${runName}_nanopolish
cd ${runName}_nanopolish

python3 /mnt/data/soft/bioinformatics_mmm_covid/bin/fixSeqSum.py -r ${runName} -g ${grid}

crumpit runstats -s ${runName}\
        -o ${runName} \
        -d \
        -u crumpit \
        -ps CrumpitUserP455! \
        -p 3306 \
        -ip 163.1.213.19

python3 /mnt/data/soft/bioinformatics_mmm_covid/bin/domains2cogmeta.py \
	${runName}_domains.csv ${runName}_meta.csv

nextflow run /mnt/data/soft/bioinformatics_mmm_covid/main.nf \
	--run $runName \
	--infolder $data/$grid/$runName/basecalled_fastq/ \
	--f5folder $data/$grid/$runName/f5s/ \
	--seqsum $PWD/sequencing_summary.txt \
	--artic_repo /mnt/data/test_data/soft/artic-ncov2019/ \
	--meta $PWD/${runName}_meta.csv \
	--VOCs $VOCs \
	-profile ndmdev3 -resume

# transfer data
#echo "put -r ${runName} /data/"| sftp  -P 8448 covidPheOxford@129.67.88.83
cd ../
