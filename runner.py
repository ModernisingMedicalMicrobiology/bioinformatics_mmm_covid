#!/usr/bin/env python3
import sys
import os
from argparse import ArgumentParser, SUPPRESS
import subprocess 


def run(opts):
    runs=opts.runs
    if (runs)==1:
        multirun='false'
        infolder='{0}/{1}/basecalled_fastq/*.gz'.format(opts.folder,runs[0])
        f5folder='{0}/{1}/f5s/*.fast5'.format(opts.folder,runs[0])
        seqsum='{0}/{1}/sequencing_summary.txt'.format(opts.folder,runs[0])
    else: 
        multirun='true'
        l,f5,ss=[],[],[]
        for run in runs:
            l.append('{0}/{1}/basecalled_fastq/*.gz'.format(opts.folder,run))
            f5.append('{0}/{1}/f5s/*.fast5'.format(opts.folder,run))
            ss.append('{0}/{1}/sequencing_summary.txt'.format(opts.folder,run))
        infolder=','.join(l)
        f5folder=','.join(f5)
        seqsum=','.join(ss)

    commands=['nextflow', 'run', '{0}/main_muti.nf'.format(opts.repo), 
        '--run', opts.combinedName,
        '--infolder', infolder,
        '--f5folder', f5folder,
        '--seqsum', seqsum,
        '--multiRun', multirun,
        '--artic_repo', opts.artic, 
        '-profile', 'slurm', '-resume']

    cwd='{0}/{1}_{2}'.format(opts.workfol,opts.combinedName,opts.variantCaller)
    print(cwd)
    if not os.path.exists(cwd):
        os.makedirs(cwd)



    print(commands)
    print(' '.join(commands))
    subprocess.run(commands, check=True, cwd=cwd)
    

if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='nextflow runner artic covid workflow on analysis1')
    parser.add_argument('-c', '--combinedName', required=True,
                             help='name of all the runs combined')
    parser.add_argument('-r', '--runs', required=True, nargs='+',
                             help='run names')
    parser.add_argument('-f', '--folder', required=False, default='/mnt/microbio/Nanopore',
                             help='root folder of runs')
    parser.add_argument('-g', '--repo', required=False, default='/mnt/microbio/HOMES/nick/soft/bioinformatics_mmm_covid/',
                             help='repo of the nextflow folder')
    parser.add_argument('--artic', required=False, default='/mnt/microbio/HOMES/nick/soft/artic-ncov2019/',
                             help='repo of the artic pipeline')
    parser.add_argument('--workfol', required=False, default='/mnt/microbio/HOMES/nick/COVID/artic_pipeline',
                             help='working folder')
    parser.add_argument('--variantCaller', required=False, default='nanopolish',
                             help='which variant caller to use')
    opts, unknown_args = parser.parse_known_args()
    run(opts)

