# bioinformatics_MMM_COVID

ARTIC network bioinformatics on MMM infrastructure.

## Overview
Here are some commands used to analyse nanopore sequences from the ARTIC protocol. It includes loging into analysis1, downloadin test data, loading the software, running on data generated from the crumpit workflow.

### login to analysis
You'll need an SSH client on your local machine such as terminal for mac and putty for windows.

```
ssh -p8081 "NDM\you"@analysis1.mmmoxford.uk
``` 

Its much easier to add this snippet to a `~/.ssh/config`

```
Host csana1
	User NDM\nick
	Hostname analysis1.mmmoxford.uk 
	Port 8081
``` 

Then you just need to type

```
ssh csana1
```

### Download some test data

Make a new folder then use wget to download some data

```
mkdir test_data
cd test_data
wget https://artic.s3.climb.ac.uk/GunIt_nb_serial_V3It.tgz
tar -xvf GunIt_nb_serial_V3It.tgz
```

### download the software repo 
Although we are going to run the software from a container, we need a writeable version of the git repository. So we're going to make a software folder and clone the git repo to it.

```
cd
mkdir soft
cd soft
git clone https://github.com/artic-network/artic-ncov2019.git
```

### Load the artic software from our singularity image
Because we were unable to install from conda on analysis1, we've put the software into a singularity container. This contains all the artic software, but changes the terminal a little bit, so you might want to open a different terminal for this bit.

```
singularity shell /mnt/microbio/HOMES/nick/articimg-2020-11-09-633818b9d54a.simg
source activate artic
```

### run the guppyplex command on the test data we just downloaded
We are going to run with the first barcode for now. You can try with all the other barcodes later. We'll make a new folder for the analysis.

```
cd test_data
mkdir analysis
cd analysis
artic guppyplex \
	--skip-quality-check \
	--min-length 400 \
	--max-length 700 \
	--directory ~/test_data/GunIt_nb_serial_V3It/GunIt_nb_serial_V3It/20200522_1258_X1_FAN36606_9a1fb784/fastq_pass/barcode01  \
	--prefix barcode01
```

### Run the minion pipeline
Next we'll run the main artic pipeline. We're going to use medaka for variant calling because its just as good and a lot faster. We also don't have fast5 files for these test datasets.
```
artic minion \
	--medaka \
	--normalise 200 \
	--threads 4 \
	--scheme-directory ~/soft/artic-ncov2019/primer_schemes \
	--read-file barcode01_barcode01.fastq \
	nCoV-2019/V3  barcode01
```

This generates some output files, including bam files, and the consensus sequence `barcode01.consensus.fasta`

```
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  50M Nov 10 09:14 barcode01.alignreport.er
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local 105M Nov 10 09:14 barcode01.alignreport.txt
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local 884M Nov  9 19:32 barcode01_barcode01.fastq
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local 122K Nov 10 09:16 barcode01-barplot.png
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  67K Nov 10 09:16 barcode01-boxplot.png
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  30K Nov 10 09:16 barcode01.consensus.fasta
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local   39 Nov 10 09:15 barcode01.coverage_mask.txt
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local 928K Nov 10 09:15 barcode01.coverage_mask.txt.nCoV-2019_1.depths
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local 928K Nov 10 09:15 barcode01.coverage_mask.txt.nCoV-2019_2.depths
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local 1.8K Nov 10 09:15 barcode01.fail.vcf
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local 3.6K Nov 10 09:15 barcode01.longshot.vcf
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  462 Nov 10 09:15 barcode01.merged.vcf.gz
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  126 Nov 10 09:15 barcode01.merged.vcf.gz.tbi
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local 4.0K Nov 10 09:16 barcode01.minion.log.txt
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  60K Nov 10 09:16 barcode01.muscle.in.fasta
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  60K Nov 10 09:16 barcode01.muscle.out.fasta
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local 2.2M Nov 10 09:14 barcode01.nCoV-2019_1.hdf
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  646 Nov 10 09:14 barcode01.nCoV-2019_1.vcf
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local 2.3M Nov 10 09:15 barcode01.nCoV-2019_2.hdf
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  461 Nov 10 09:15 barcode01.nCoV-2019_2.vcf
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  917 Nov 10 09:16 barcode01.pass.vcf.gz
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  126 Nov 10 09:16 barcode01.pass.vcf.gz.tbi
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  30K Nov 10 09:16 barcode01.preconsensus.fasta
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  340 Nov 10 09:15 barcode01.primers.vcf
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  11M Nov 10 09:14 barcode01.primertrimmed.nCoV-2019_1.sorted.bam
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  128 Nov 10 09:14 barcode01.primertrimmed.nCoV-2019_1.sorted.bam.bai
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  11M Nov 10 09:14 barcode01.primertrimmed.nCoV-2019_2.sorted.bam
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  152 Nov 10 09:14 barcode01.primertrimmed.nCoV-2019_2.sorted.bam.bai
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  21M Nov 10 09:14 barcode01.primertrimmed.rg.sorted.bam
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  152 Nov 10 09:14 barcode01.primertrimmed.rg.sorted.bam.bai
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local 373M Nov 10 09:11 barcode01.sorted.bam
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  200 Nov 10 09:11 barcode01.sorted.bam.bai
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  21M Nov 10 09:12 barcode01.trimmed.rg.sorted.bam
-rw-r--r-- 1 nick@ndm.local domain users@ndm.local  168 Nov 10 09:14 barcode01.trimmed.rg.sorted.bam.bai
```

### Downloading the data
You might want to look at some files, for example the `barcode01-barplot.png`

This is where adding the host to the `.ssh/config` is most useful. Here you can type this rsync command on your local machine.

```
rsync -avP csana1:~/test_data/analysis/barcode01-barplot.png ./
```

### plotting coverage
I couldn't see a quick way to visualise the bam file in the artic software, but I've written a very small python script to it.

```python
import pysam
import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

def getStats(bam):
        keys=['chrom','length','map','unmap']
        d=dict(zip(keys,pysam.idxstats(bam).split('\n')[0].split('\t')))
        return d

def getCovs(bam):
        bd=getStats(bam)
        cov=[]
        samfile = pysam.AlignmentFile(bam, "rb")
        n=0
        for pileupcolumn in samfile.pileup(bd['chrom'], 0, int(bd['length'])):
                if pileupcolumn.pos > n:
                        for i in range(int(pileupcolumn.pos)-n):
                                cov.append(0)
                        n=pileupcolumn.pos
                cov.append(pileupcolumn.n)
                n+=1
        return cov, bd



def smooth(li,win):
        r=len(li)
        a=[sum(li[i:i+win])/float(win) for i in range(0,r-win,int(r/100.0))]
        return a

def plotCov(co,bd):
        r,i=getName(sys.argv[1])
        #co2=smooth(co,100)
        co2=co
        #plt.plot(co)
        plt.plot(co2)
        plt.title("Sample {0}, ref {1}".format(i,bd['chrom']))
        plt.xlabel("Position on reference")
        plt.ylabel("Coverage depth (bp)")
        #plt.show()
        plt.savefig("Sample_{0}_ref_{1}.pdf".format(i,bd['chrom']))


def getName(s):
        s=s.split('/')
        ref=s[-1].replace(".sorted.bam","")
        i=s[-1].split('.')[0]
        return ref,i

c,bd=getCovs(sys.argv[1])
print('Ref length','total bases','average depth')
print(len(c),sum(c),sum(c)/float(len(c)))
plotCov(c,bd)
```

But I've already added it to everyones path on analysis1, so it can be run like this.

```
plotBam.py barcode01.sorted.bam
```

This generates a file `Sample_barcode01_ref_MN908947.3.pdf` that you can download using rsync as above.

## Running on MMM generated data
Data generated here in MMM gets moved to this location `/mnt/microbio/Nanopore/`

Sheila's first run is therefore here: `/mnt/microbio/Nanopore/COVID_ARTIC_1/basecalled_fastq/`

We store all the barcodes in the same folder and this doesn't work with the artic pipeine, so I've written a script to symlink the files to a new directory

```python
#!/usr/bin/env python3
import os
import sys

infol=sys.argv[1]
outfol=sys.argv[2]

f=os.listdir(infol)

d={}
for i in f:
    d.setdefault(i.split('_')[0],[]).append(i)

def makeFol(f):
    try:
        os.mkdir(f)
    except:
        pass

makeFol(outfol)

for barcode in d:
    out='{0}/{1}'.format(outfol,barcode)
    makeFol(out)
    for fi in d[barcode]:
        os.symlink('{0}/{1}'.format(infol, fi),
                '{0}/{1}'.format(out,fi) )
```

This can be run as such, I've already added it to everyones path:

```
cd
mkdir COVID_ARTIC_1_analysis
cd COVID_ARTIC_1_analysis
lnBarcodes.py /mnt/microbio/Nanopore/COVID_ARTIC_1/basecalled_fastq/ barcodes 
```

Then we can run same commands as before:

```
artic guppyplex \
        --skip-quality-check \
        --min-length 400 \
        --max-length 700 \
        --directory barcodes/barcode01/ \
        --prefix barcode01


artic minion \
        --medaka \
        --normalise 200 \
        --threads 4 \
        --scheme-directory ~/soft/artic-ncov2019/primer_schemes \
        --read-file barcode01_barcode01_.fastq \
        nCoV-2019/V3  barcode01

python3 plotBam.py barcode01.sorted.bam
```

## Using nextflow 
I have created a simple nextflow workflow that can run these tasks

```bash
nextflow run main.nf \
	--run COVID_ARTIC_2 \
	--infolder /mnt/nanostore/grid0/COVID_ARTIC_2/basecalled_fastq \
	-profile standard 
```
