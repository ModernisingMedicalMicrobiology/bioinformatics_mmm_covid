params.infolder=''
params.f5folder=''
params.seqsum=''
params.multiRun=false
params.run=''
params.artic_repo=''
params.scheme='nCoV-2019/V3'
params.rawGrid == false


if (params.multiRun == false){
if (params.rawGrid == true){
Channel
    .fromPath("$params.infolder/**/*")
    .map {file -> tuple(file.parent.name, file) }
    .groupTuple(by: 0)
    .set{ bars }

Channel
    .fromPath("$params.f5folder/**/*.fast5")
    .map {file -> tuple(file.parent.name, file) }
    .groupTuple(by: 0)
    .set{ fast5s }

Channel                                                                         
    .fromPath("$params.seqsum/*equencing_summary.txt", type:'file')                                          
    .collect()                                                                  
    .set{ seqsums2 }                                                           
process mergeSeqSums {
    input:
    file('*.txt') from seqsums2

    output:
    file('seqsum') into seqsums

    script:
    """
    head -n 1 1.txt > seqsum
    tail -q -n +2 *.txt >> seqsum
    """
}
}
else {
Channel
    .fromPath("$params.infolder/*.gz")
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .view()
    .into{ bars }

Channel
    .fromPath("$params.f5folder/*.fast5")
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .view()
    .into{ fast5s }
Channel
    .fromPath("$params.seqsum")
    .into{ seqsums }
}}
else {
fqList = params.infolder?.tokenize(',')
f5List = params.f5folder?.tokenize(',')
seqsumList = params.seqsum?.tokenize(',')
Channel
    .fromPath(fqList, type:'file')
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .view()
    .into{ bars }

Channel
    .fromPath(f5List, type:'file')
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .view()
    .into{ fast5s }
Channel
    .fromPath(seqsumList, type:'file')
    .collect()
    .into{ seqsums2 }

process mergeSeqSums {
    input:
    file('*.txt') from seqsums2

    output:
    file('seqsum') into seqsums

    script:
    """
    head -n 1 1.txt > seqsum
    tail -q -n +2 *.txt >> seqsum
    """
}
}

process guppyplex {
    tag {barcode}

    input:
    set val(barcode), file('*.fastq.gz') from bars

    output:
    set val(barcode), file("${barcode}_.fastq") into plexed

    """
    artic guppyplex \
        --skip-quality-check \
        --min-length 400 \
        --max-length 700 \
        --directory ./ \
        --prefix ${barcode}
    """

}

process reduceSums {
    tag {barcode}
                                                          
    input:                                                                      
    set val(barcode), file("${barcode}_.fastq"), file('seqsum.txt') from plexed.combine(seqsums)                                                 
    output:                                                                     
    set val(barcode), file("${barcode}_.fastq"), file('sequencing_summary.txt') into fastqSum                                                                            
    script:                                                                     
    """ 
    reduceSeqSum.py -f *.fastq -s *.txt                                                                        
    """                                                                         
}

process artic_minion {
    errorStrategy 'ignore'
    tag {barcode}
    cpus=2

    publishDir "pngs/", mode: 'copy', pattern: '*png'
    publishDir "consensus_seqs/", mode: 'copy', pattern: '*fasta'
    publishDir "bams/", mode: 'copy', pattern: '*bam'
    publishDir "VCFs/", mode: 'copy', pattern: '*.vcf*'

    input:                                                                      
    set val(barcode), file("${barcode}_.fastq"),file('sequencing_summary.txt'), file("*.fast5") from fastqSum.combine(fast5s, by: 0)
//    file('seqsum.txt') from seqsums

    output:
    set val(barcode), file("${run}_${barcode}.sorted.bam") into (bams,bams2)
    set val(barcode), file("*.png") into pngs
    file("*.vcf*") into vcfs
    file("${run}_${barcode}.consensus.fasta") into (conSeqs, conSeqs2)
    file("${run}_${barcode}.primertrimmed.rg.sorted.bam") into primerTrimmed
    script:
    run=params.run
    repo=params.artic_repo
    scheme=params.scheme
    """
    source activate artic
    export LD_LIBRARY_PATH=/opt/conda/envs/artic/lib/
    artic minion \
        --normalise 200 \
        --threads ${task.cpus} \
        --scheme-directory ${repo}/primer_schemes \
        --read-file ${barcode}_.fastq \
        --fast5-directory ./ \
        --sequencing-summary sequencing_summary.txt \
        ${scheme}  ${run}_${barcode}
    """
}

process pysamStats {                                                              
    tag { barcode }                                                                  
                                                                                    
    publishDir 'pysamStats',mode: 'copy'                                                      
                                                                                    
    input:                                                                          
    set val(barcode), file("sorted.bam") from bams2
                                                                                    
    output:                                                                         
    set val(barcode), file("${run}_${barcode}_pysam.tsv") into pStats                            
                                                                                    
    script:                                         
    run=params.run 
    repo=params.artic_repo                                                      
    scheme=params.scheme
    """ 
    samtools index sorted.bam 
    pysamstats -t variation_strand sorted.bam -f \
        ${repo}/primer_schemes/${scheme}/nCoV-2019.reference.fasta > ${run}_${barcode}_pysam.tsv                
    """                                                                             
} 

process bam_analysis {                                                              
tag { barcode }                                                                  
                                                                                
publishDir 'coverageStats', mode: 'copy'                                        
                                                                                
input:                                                                          
set val(barcode), file("sorted.bam") from bams
                                                                                
output:                                                                         
set val(barcode), file("${run}_${barcode}.csv") into analysis                            
                                                                                
script:                                         
run=params.run                                
"""                  
samtools index sorted.bam                                                           
samtools depth -aa sorted.bam > depth.tsv                   

bamStats.py depth.tsv ${run}_${barcode} None                             

"""                                                                             
} 

process pangolin {
    tag {run}

    publishDir 'pangolin', mode: 'copy'

    input:
    file('*.fasta') from conSeqs.collect()

    output:
    file("lineage_report.csv") into pangolout

    script:
    repo=params.artic_repo
    """
    cat *.fasta > unaligned.fasta
    pangolin unaligned.fasta
    """
}            

process nextclade {                                                             
    tag {run}                                                                   
                                                                                
    publishDir 'nextclade', mode: 'copy'                                         
                                                                                
    input:                                                                      
    file('*.fasta') from conSeqs2.collect()                                      
                                                                                
    output:                                                                     
    set file("${run}_tree.json"), file("${run}.tsv"), file("${run}.tsv") into ncOut
                                                                                
    script:                                                                     
    run=params.run                                                              
    """                                                                         
    cat *.fasta > sequences.fasta                                               
    nextclade --input-fasta 'sequences.fasta' \
        --output-tree ${run}_tree.json \
        --output-tsv ${run}.tsv \
        --output-json ${run}.json                                               
    """                                                                         
}                                   
