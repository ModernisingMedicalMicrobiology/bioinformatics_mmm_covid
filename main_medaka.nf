params.infolder=''
params.run=''
params.artic_repo=''
params.scheme='nCoV-2019/V3'
params.medaka_model='r941_min_high_g360'

Channel
    .fromPath("$params.infolder/*.gz")
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .view()
    .into{ bars }

process guppyplex {
    tag {barcode}

    input:
    set val(barcode), file('*.fastq.gz') from bars

    output:
    set val(barcode), file("${barcode}_.fastq") into plexed

    """
    source activate artic
    artic guppyplex \
        --skip-quality-check \
        --min-length 400 \
        --max-length 700 \
        --directory ./ \
        --prefix ${barcode}
    """

}

process artic_minion {
    errorStrategy 'ignore'
    tag {barcode}
    cpus=2

    publishDir "pngs/", mode: 'copy', pattern: '*png'
    publishDir "consensus_seqs/", mode: 'copy', pattern: '*fasta'
    publishDir "bams/", mode: 'copy', pattern: '*bam'
    publishDir "VCFs/", mode: 'copy', pattern: '*.vcf*'

    input:                                                                      
    set val(barcode), file("${barcode}_.fastq") from plexed

    output:
    set val(barcode), file("${run}_${barcode}.sorted.bam") into (bams,bams2)
    set val(barcode), file("*.png") into pngs
    file("*.vcf*") into vcfs
    file("${run}_${barcode}.consensus.fasta") into (conSeqs,conSeqs2)
    script:
    run=params.run
    repo=params.artic_repo
    scheme=params.scheme
    model=params.medaka_model
    """
    artic minion \
        --medaka \
        --normalise 200 \
        --threads ${task.cpus} \
        --medaka-model ${model} \
        --scheme-directory ${repo}/primer_schemes \
        --read-file ${barcode}_.fastq \
        ${scheme}  ${run}_${barcode}
    """
}

process pysamStats {                                                              
    tag { barcode }                                                                  
                                                                                    
    publishDir 'pysamStats',mode: 'copy'                                                      
                                                                                    
    input:                                                                          
    set val(barcode), file("sorted.bam") from bams2
                                                                                    
    output:                                                                         
    set val(barcode), file("${run}_${barcode}_pysam.tsv") into pStats                            
                                                                                    
    script:                                         
    run=params.run 
    repo=params.artic_repo                                                      
    scheme=params.scheme
    """ 
    samtools index sorted.bam 
    pysamstats -t variation_strand sorted.bam -f \
        ${repo}/primer_schemes/${scheme}/nCoV-2019.reference.fasta > ${run}_${barcode}_pysam.tsv                
    """                                                                             
} 

process bam_analysis {                                                              
tag { barcode }                                                                  
                                                                                
publishDir 'coverageStats', mode: 'copy'                                        
                                                                                
input:                                                                          
set val(barcode), file("sorted.bam") from bams
                                                                                
output:                                                                         
set val(barcode), file("${run}_${barcode}.csv") into analysis                            
                                                                                
script:                                         
run=params.run                                
"""                  
samtools index sorted.bam                                                           
samtools depth -aa sorted.bam > depth.tsv                   
bamStats.py depth.tsv ${run}_${barcode} None                             
"""                                                                             
} 

process pangolin {
    tag {run}

    publishDir 'pangolin', mode: 'copy'

    input:
    file('*.fasta') from conSeqs.collect()

    output:
    file("lineage_report.csv") into pangolout

    script:
    repo=params.artic_repo
    """
    cat *.fasta > unaligned.fasta
    mafft --auto  unaligned.fasta > aligned.fasta
    pangolin aligned.fasta
    """
}            

process nextclade {
    tag {run}
    cpus=4

    publishDir 'nextclade', mode: 'copy'

    input:
    file('*.fasta') from conSeqs2.collect()

    output:
    set file("${run}_tree.json"), file("${run}.tsv"), file("${run}.tsv") into ncOut 

    script:
    run=params.run
    """
    cat *.fasta > sequences.fasta
    nextclade -j ${task.cpus} --input-fasta 'sequences.fasta' \
        --output-tree ${run}_tree.json \
        --output-tsv ${run}.tsv \
        --output-json ${run}.json
    """
}            
