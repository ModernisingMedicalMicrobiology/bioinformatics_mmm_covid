params.infolder=''
params.f5folder=''
params.seqsum=''
params.multiRun=false
params.run=''
params.artic_repo=''
params.scheme='nCoV-2019/V3'



if (params.multiRun == false){
Channel
    .fromPath("$params.infolder/*.gz")
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .view()
    .into{ bars }

Channel
    .fromPath("$params.f5folder/*.fast5")
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .view()
    .into{ fast5s }
Channel
    .fromPath("$params.seqsum")
    .into{ seqsums }
}
else {
fqList = params.infolder?.tokenize(',')
f5List = params.f5folder?.tokenize(',')
seqsumList = params.seqsum?.tokenize(',')
Channel
    .fromPath(fqList, type:'file')
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .view()
    .into{ bars }

Channel
    .fromPath(f5List, type:'file')
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .view()
    .into{ fast5s }
Channel
    .fromPath(seqsumList, type:'file')
    .collect()
    .into{ seqsums2 }

process mergeSeqSums {
    input:
    file('*.txt') from seqsums2

    output:
    file('seqsum') into seqsums

    script:
    """
    head -n 1 1.txt > seqsum
    tail -q -n +2 *.txt >> seqsum
    """
}
}

process minimap2 {
    tag {barcode}
    cpus=2
    input:
    set val(barcode), file('*.fastq.gz') from bars

    output:
    set val(barcode), file("${run}_${barcode}.sorted.bam") into (bams,bams2)

    script:
    run=params.run
    repo=params.artic_repo
    scheme=params.scheme
    """
    minimap2 -t ${task.cpus}  -ax map-ont \
        ${repo}/primer_schemes/${scheme}/nCoV-2019.reference.fasta \
        *.fastq.gz | samtools view -bS -F4 -o ${barcode}.bam -  

    samtools sort ${barcode}.bam -o ${run}_${barcode}.sorted.bam
    """

}


process pysamStats {                                                              
    tag { barcode }                                                                  
                                                                                    
    publishDir 'pysamStats',mode: 'copy'                                                      
                                                                                    
    input:                                                                          
    set val(barcode), file("sorted.bam") from bams2
                                                                                    
    output:                                                                         
    set val(barcode), file("${run}_${barcode}_pysam.tsv") into pStats                            
                                                                                    
    script:                                         
    run=params.run 
    repo=params.artic_repo                                                      
    scheme=params.scheme
    """ 
    samtools index sorted.bam 
    pysamstats -t variation_strand sorted.bam -f \
        ${repo}/primer_schemes/${scheme}/nCoV-2019.reference.fasta > ${run}_${barcode}_pysam.tsv                
    """                                                                             
} 

process bam_analysis {                                                              
tag { barcode }                                                                  
                                                                                
publishDir 'coverageStats', mode: 'copy'                                        
                                                                                
input:                                                                          
set val(barcode), file("sorted.bam") from bams
                                                                                
output:                                                                         
set val(barcode), file("${run}_${barcode}.csv") into analysis                            
                                                                                
script:                                         
run=params.run                                
"""                  
samtools index sorted.bam                                                           
samtools depth -aa sorted.bam > depth.tsv                   

bamStats.py depth.tsv ${run}_${barcode} None                             

"""                                                                             
} 

