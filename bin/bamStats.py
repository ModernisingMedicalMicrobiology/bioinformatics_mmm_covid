#!/usr/bin/env python3
import pandas as pd
import sys

depthFile=sys.argv[1]
df=pd.read_csv(depthFile,names=['chrom','pos','depth'],sep='\t')

d={}

d['refAcc']=df['chrom'].unique()[0]
d['baseMapped']=df['depth'].sum()
d['aveCov']=df['depth'].mean()
# number of positions with coverage above 1
x1 = df['pos'][df['depth']>=1].count()
d['cov1Rate']= (x1/df['pos'].max())*100
# Same for coverage above 5
x5 = df['pos'][df['depth']>=5].count()
d['cov5Rate'] = (x5/df['pos'].max())*100
# Same for coverage above 20 
x20 = df['pos'][df['depth']>=20].count()
d['cov20Rate'] = (x20/df['pos'].max())*100

d['Sample ID']=sys.argv[2]
d['Sample Name']=sys.argv[3]

df2=pd.DataFrame([d])#,columns=['Sample Name'])
print(df2)

df2.to_csv('{}.csv'.format(sys.argv[2]),index=False)
