#!/usr/bin/env python3
import pandas as pd
import sys
from argparse import ArgumentParser, SUPPRESS
import os
import subprocess

def getNewName(r):
    newName=r['barcode'] + '_' + r['batch'] + '_0.fast5'
    return newName

def getOldName(r):
    oldName=r['f5'].split('/')[-1]
    return oldName

def getNewNameFQ(r):
    newName=r['barcode'] + '_' + r['batch'] + '.fastq'
    return newName

def getOldNameFQ(r):
    oldName=r['fastq'].split('/')[-1]
    return oldName

def getSeqSum(grid, run):
    fol='/mnt/{0}/{1}/'.format(grid,run)
    out=subprocess.check_output(['find', fol, '-name', 'sequencing_summary*.txt'],universal_newlines=True)
    out=out.splitlines()
    dfs=[]
    for f in out:
        dfs.append(pd.read_csv(f,sep='\t'))
    if len(dfs)>0:
        df=pd.concat(dfs)
    else:
        out=subprocess.check_output(['find', fol, '-name', 'fastq_pass'],universal_newlines=True)
        out=out.splitlines()
        out[0]
        ch=out[0].split('/')[-2].split('_')[2]
        fol='/mnt/{0}/reads/tmp/{1}/{2}/'.format(grid,ch,run)
        out=subprocess.check_output(['find', fol, '-name', 'sequencing_summary*.txt.tmp'],universal_newlines=True)
        out=out.splitlines()
        for f in out:
            dfs.append(pd.read_csv(f,sep='\t'))
        df=pd.concat(dfs)
    return df

def getMeta(grid,run):
    batches=os.listdir('/mnt/nanostore/{0}/{1}/all_batches/'.format(grid,run))
    dfs=[]
    for batch in batches:
        bf='/mnt/nanostore/{0}/{1}/all_batches/{2}'.format(grid,run,batch)
        dfs.append(pd.read_csv(bf,names=['device','batch','f5','fastq','Na','barcode']))
    df=pd.concat(dfs)
    return df

def upload(grid,run):
     seq_sum='/mnt/nanostore/{0}/{1}/sequencing_summary.txt'.format(grid,run)
     uploadLoc='/mnt/microbio/Nanopore/{0}/'.format(run)
     rsyncCommand=['rsync', '-avP', seq_sum, 'csslurmh:{0}'.format(uploadLoc)]
     print(rsyncCommand)

     try:
         subprocess.run(rsyncCommand,shell=False, check=False)
     except subprocess.CalledProcessError as e:
         print(e.output)


def run(opts):
    grid=opts.grid
    run=opts.runName
    meta=getMeta(grid, run)
    meta['new f5']=meta.apply(getNewName,axis=1)
    meta['old f5']=meta.apply(getOldName,axis=1)
    meta['new fq']=meta.apply(getNewNameFQ,axis=1)
    meta['old fq']=meta.apply(getOldNameFQ,axis=1)
    
    dF5=pd.Series(meta['new f5'].values,index=meta['old f5']).to_dict()
    dFQ=pd.Series(meta['new fq'].values,index=meta['old fq']).to_dict()
    
    print('Reading sequencing summary file')
    df=getSeqSum(grid,run)
    print('changing f5 file names')
    df['filename_fast5']=df['filename_fast5'].map(dF5)
    print('changing fq file names')
    df['filename_fastq']=df['filename_fastq'].map(dFQ)
    print('writing to file')
    df.dropna(inplace=True)
    seq_sum='/mnt/data/pipelines/artic/{0}_nanopolish/sequencing_summary.txt'.format(run)
    df.to_csv(seq_sum, sep='\t', index=False)
    if opts.upload == True:
        upload(grid,run)

if __name__ == "__main__":
    # args
    parser = ArgumentParser(description='Rename file names in sequencing summary file')
    parser.add_argument('-r', '--runName', required=True,
                             help='Name the run')
    parser.add_argument('-g', '--grid', required=True, 
                             help='gridion used')
    parser.add_argument('-u', '--upload', required=False,action='store_true', 
                             help='toggle on to upload to folder on computer science')
    opts, unknown_args = parser.parse_known_args()
    
    run(opts) 


