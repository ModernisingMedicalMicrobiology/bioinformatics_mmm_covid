#!/usr/bin/env python3
import pandas as pd
import sys

df=pd.read_csv(sys.argv[1])
df=df[df['Barcode number']!='unclassified']
df['Barcode']='NB'+df['Barcode number'].str.zfill(2)
df['Cog']=df['Sample name']
df['Run']=df['Run name']
df=df[['Cog','Barcode','Run']]
df.to_csv(sys.argv[2],index=False)
