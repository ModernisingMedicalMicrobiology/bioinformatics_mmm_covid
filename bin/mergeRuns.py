#!/usr/bin/env python3
import os
import sys
import pandas as pd

def makeFol(f):
    try:
        os.makedirs(f)
    except:
        pass

runName='COVID_locost_14'
runs=[]


#df=pd.read_excel('../../analysis/200203 metadata.xlsx')
df=pd.read_csv('../../analysis/200203 metadata.csv')
df=df[df['run']==runName]
df.dropna(subset=['previous_bc','run'],inplace=True)
df['barcode']=df['barcode'].map(int)
df['barcode']=df['barcode'].apply('{:0>2}'.format)
df['previous_bc']=df['previous_bc'].map(int)
df['previous_bc']=df['previous_bc'].apply('{:0>2}'.format)
makeFol(runName)

## get all runs
altRuns={}
runs=os.listdir('/mnt/microbio/Nanopore/')
altRuns[runName]=[run for run in runs if run.startswith(runName)]
previous_runs=df['previous_run'].unique()

## add run if it doesn't have an integer after the replaced name, i.e stop locost_1 pairing with locost_11
for prun in previous_runs:
    print(prun)
    for run in runs:
        if prun == run:
            print(prun, run)
            altRuns.setdefault(prun,[]).append(run)
            continue
        if run.startswith(prun):
            suffix=run.replace(prun,'')[0]
            print(prun, run, suffix.isdigit())
            if suffix.isdigit() == False:
                altRuns.setdefault(prun,[]).append(run)


# Get all fastqs for each run
f1,f2,d1,d2={},{},{},{}
for run in altRuns[runName]:
    infol1='/mnt/microbio/Nanopore/{0}/basecalled_fastq/'.format(run)
    r1Files=os.listdir(infol1)
    for i in r1Files:
        d1.setdefault((run, i.split('_')[0]), []).append(('{0}/{1}'.format(infol1,i),i))
    infol1='/mnt/microbio/Nanopore/{0}/f5s/'.format(run)
    r1Files=os.listdir(infol1)
    for i in r1Files:
        f1.setdefault((run, i.split('_')[0]), []).append(('{0}/{1}'.format(infol1,i),i))
for comRun in df['previous_run'].unique():
    for run in altRuns[comRun]:
        infol1='/mnt/microbio/Nanopore/{0}/basecalled_fastq/'.format(run)
        r1Files=os.listdir(infol1)
        for i in r1Files:
            d2.setdefault((run,i.split('_')[0]), []).append(('{0}/{1}'.format(infol1,i),i)) 
        infol1='/mnt/microbio/Nanopore/{0}/f5s/'.format(run)
        r1Files=os.listdir(infol1)
        for i in r1Files:
            f2.setdefault((run,i.split('_')[0]), []).append(('{0}/{1}'.format(infol1,i),i)) 

for i,r in df.iterrows():
    barcode='barcode{0}'.format(r['barcode'])
    barcode2='barcode{0}'.format(r['previous_bc'])

    outFol='{0}/fastq_pass/{1}'.format(runName,barcode)
    makeFol(outFol)
    outFol5='{0}/fast5_pass/{1}'.format(runName,barcode)
    makeFol(outFol5)

    # link runs from first runName
    for run in altRuns[runName]:
        try:
            seqsum='/mnt/microbio/Nanopore/{0}/sequencing_summary.txt'.format(run)
            seqsumOut='{0}/{1}_sequencing_summary.txt'.format(runName,run)
            os.symlink(seqsum,seqsumOut) 
        except:
            pass

        for f in d1[(run, barcode)]:
#            print(r['run'],barcode, f[0])
            outF='{0}/{1}_{2}'.format(outFol, run, f[1])
            os.symlink(f[0], outF)
        for f in f1[(run, barcode)]:
#            print(r['run'],barcode, f[0])
            outF='{0}/{1}_{2}'.format(outFol5, run, f[1])
            os.symlink(f[0], outF)
    for run in altRuns[r['previous_run']]:
        try:
            seqsum='/mnt/microbio/Nanopore/{0}/sequencing_summary.txt'.format(run)
            seqsumOut='{0}/{1}_sequencing_summary.txt'.format(runName,run)
            os.symlink(seqsum,seqsumOut)
        except:
            pass
        if (run, barcode2) in d2: 
            for f in d2[(run, barcode2)]:
                outF='{0}/{1}_{2}'.format(outFol, run, f[1])
                os.symlink(f[0], outF)    
            for f in f2[(run, barcode2)]:
                outF='{0}/{1}_{2}'.format(outFol5, run, f[1])
                os.symlink(f[0], outF)    


