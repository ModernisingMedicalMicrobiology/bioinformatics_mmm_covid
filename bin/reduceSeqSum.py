#!/usr/bin/env python3
import pandas as pd
from Bio import SeqIO
import argparse
import gzip
from multiprocessing import Pool                                                                            


class reduceSeqs:
    def __init__(self, fastqs, seqsums, chunksize=100000, threads=4):
        self.fastqs=fastqs
        self.seqsums=seqsums
        self.chunksize=chunksize
        self.threads=threads
        self.chunks=0 
        print('getting list of IDs')
        self.seqIDs=list(self.getIDs())
        print(self.seqIDs[0:20])
        print('reading seq sumss')
        self.reduceSeqSum()
    
    def getIDs(self):
        for fq in self.fastqs:
            for seq in SeqIO.parse(open(fq,'rt'), 'fastq'):
                yield seq.id
    
    def reduceSeq(self, df):
        self.chunks+=1
        df=df[df['read_id'].isin(self.seqIDs)]
        print(self.chunks,len(df))
        return df

    def reduceSeqSum(self):
        dfs=[]
        chunks=0
        for seqsum in self.seqsums:
            df_iter=pd.read_csv(seqsum, sep="\t",iterator=True, chunksize=self.chunksize)
#            with Pool(4) as p:
#                dfs=p.map(self.reduceSeq, df_iter)
    
            for df in df_iter:
                chunks+=1
                df=df[df['read_id'].isin(self.seqIDs)]
                print(chunks,len(df))
                dfs.append(df)
        df=pd.concat(dfs)
        print('writing seq sums')
        df.to_csv('sequencing_summary.txt',sep='\t',index=False)

def run(opts):
    print('getting list of IDs')
    seqIDs=list(getIDs(opts.fastqs))
    print(seqIDs[0:20])
    print('reading seq sumss')
    reduceSeqSum(opts.seqsums,seqIDs)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--fastqs', required=True, nargs='+',
        help='fastq files')
    parser.add_argument('-s', '--seqsums', required=True, nargs='+',
        help='seqsum files')
    opts, unknown_args = parser.parse_known_args()

    reduceSeqs(opts.fastqs, opts.seqsums)
    #run(opts)
