#!/usr/bin/env python3
import os
import sys

infol=sys.argv[1]
outfol=sys.argv[2]

f=os.listdir(infol)

d={}
for i in f:
    d.setdefault(i.split('_')[0],[]).append(i)

def makeFol(f):
    try:
        os.mkdir(f)
    except:
        pass

makeFol(outfol)

for barcode in d:
    out='{0}/{1}'.format(outfol,barcode)
    makeFol(out)
    for fi in d[barcode]:
        os.symlink('{0}/{1}'.format(infol, fi),
                '{0}/{1}'.format(out,fi) )

