#!/usr/bin/env python3
import sys
import pandas as pd
from argparse import ArgumentParser


def getRun(r):
    if 'taxon' in r:
        t=r['taxon'].split('/')
    elif 'seqName' in r:
        t=r['seqName'].split('/')
    else:
        return r
    b=t[0].split('barcode')[-1]
    run=t[0].split('_barcode')[0]
    r['Run']=run#.replace('COVID_locost_','')
    try:
        r['Barcode']=int(b)
        #r['run']=r['run'].map(int)
    except:
        r['Barcode']='unclassified'
    return r

def checkVOCs(r,vocs):
    if pd.isna(r['aaSubstitutions']) == False:
        Muts=r['aaSubstitutions'].split(',')
        spikeMuts=[i[2:] for i in Muts if i[0]=='S']
    else:
        spikeMuts=[]
    for i,v in vocs.iterrows():
        if v['Lineage'] == r['lineage']:
            if pd.isna(v['Mutations'])==True:
                r['VOC']=v['Variant']
            else:
                if v['Mutations'] in spikeMuts:
                    r['VOC']=v['Variant']
                    r['VOC mutation']=v['Mutations']
    return r



def run(opts):
    meta=pd.read_csv(opts.meta)
    vocs=pd.read_csv(opts.VOCs)
    ncld=pd.read_csv(opts.nextclade,sep='\t')
    pang=pd.read_csv(opts.pangolin)
    ncld=ncld.apply(getRun, axis=1)
    pang=pang.apply(getRun, axis=1)

    pang=pang[['Run','Barcode','lineage']]
    ncld=ncld[['Run','Barcode','aaSubstitutions']]

    meta['Barcode']=meta['Barcode'].str.replace('NB','')
    meta['Barcode']=meta['Barcode'].map(int)
    df=meta.merge(pang, on=['Run','Barcode'], how='left')
    df=df.merge(ncld, on=['Run','Barcode'], how='left')

    df=df.apply(checkVOCs, axis=1, args=(vocs,))
    if not 'VOC mutation' in df.columns:
        df['VOC mutation']=None
    df=df[['Cog','Run','Barcode','VOC','lineage', 'VOC mutation']]
    df.to_csv(opts.output, index=False)


if __name__=="__main__":
    # args
    parser = ArgumentParser(description='parse VOCs, metadata, and pangolin/nextclade output')
    parser.add_argument('-m', '--meta', required=True,
                             help='meta data file')
    parser.add_argument('-v', '--VOCs', required=True,
                             help='Variants of concern csv file')
    parser.add_argument('-n', '--nextclade', required=True,
                             help='nextclade file')
    parser.add_argument('-p', '--pangolin', required=True,
                             help='pangolin file')
    parser.add_argument('-o', '--output', required=True,
                             help='output file')

    opts, unknown_args = parser.parse_known_args()
    run(opts)


