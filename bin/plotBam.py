#!/usr/bin/env python3
import pysam
import sys
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

def getStats(bam):
        keys=['chrom','length','map','unmap']
        d=dict(zip(keys,pysam.idxstats(bam).split('\n')[0].split('\t')))
        return d

def getCovs(bam):
        bd=getStats(bam)
        cov=[]
        samfile = pysam.AlignmentFile(bam, "rb")
        n=0
        for pileupcolumn in samfile.pileup(bd['chrom'], 0, int(bd['length'])):
                if pileupcolumn.pos > n:
                        for i in range(int(pileupcolumn.pos)-n):
                                cov.append(0)
                        n=pileupcolumn.pos
                cov.append(pileupcolumn.n)
                n+=1
        return cov, bd



def smooth(li,win):
        r=len(li)
        a=[sum(li[i:i+win])/float(win) for i in range(0,r-win,int(r/100.0))]
        return a

def plotCov(co,bd):
        r,i=getName(sys.argv[1])
        #co2=smooth(co,100)
        co2=co
        #plt.plot(co)
        plt.plot(co2)
        plt.title("Sample {0}, ref {1}".format(i,bd['chrom']))
        plt.xlabel("Position on reference")
        plt.ylabel("Coverage depth (bp)")
        #plt.show()
        plt.savefig("Sample_{0}_ref_{1}.pdf".format(i,bd['chrom']))


def getName(s):
        s=s.split('/')
        ref=s[-1].replace(".sorted.bam","")
        i=s[-1].split('.')[0]
        return ref,i

c,bd=getCovs(sys.argv[1])
print('Ref length','total bases','average depth')
print(len(c),sum(c),sum(c)/float(len(c)))
plotCov(c,bd)
