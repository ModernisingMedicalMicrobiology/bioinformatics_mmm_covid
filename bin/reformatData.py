#!/usr/bin/env python3
import os
import sys
import pandas as pd

def makeFol(f):
    try:
        os.makedirs(f)
    except:
        pass


def getFiles(run):
    fqs,f5s={},{}
    infol1='/mnt/microbio/Nanopore/{0}/basecalled_fastq/'.format(run)
    r1Files=os.listdir(infol1)
    for i in r1Files:
        fqs.setdefault((run, i.split('_')[0]), []).append(('{0}/{1}'.format(infol1,i),i))
    infol1='/mnt/microbio/Nanopore/{0}/f5s/'.format(run)
    r1Files=os.listdir(infol1)
    for i in r1Files:
        f5s.setdefault((run, i.split('_')[0]), []).append(('{0}/{1}'.format(infol1,i),i))

    return fqs, f5s

def symlnFiles(run, fastqs, fast5s):
    # symlink seqsum
    seqsum='/mnt/microbio/Nanopore/{0}/sequencing_summary.txt'.format(run)
    seqsumOut='{0}/sequencing_summary.txt'.format(run,run)
    os.symlink(seqsum,seqsumOut)

    # symlink fastqs
    for run,barcode in fastqs:
        outFol='{0}/fastq_pass/{1}'.format(run,barcode)
        makeFol(outFol)

        for f in fastqs[(run,barcode)]:
            outF='{0}/{1}'.format(outFol, f[1])
            os.symlink(f[0], outF)


    # symlink fast5s
    for run,barcode in fast5s:
        outFol5='{0}/fast5_pass/{1}'.format(run,barcode)
        makeFol(outFol5)

        for f in fast5s[(run,barcode)]:
            outF='{0}/{1}'.format(outFol5, f[1])
            os.symlink(f[0], outF)


def run(run):
    makeFol(run)
    fqs,f5s=getFiles(run)
    symlnFiles(run,fqs,f5s)

run(sys.argv[1])
