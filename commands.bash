#nextflow run ~/soft/bioinformatics_mmm_covid/main.nf \
#	--run COVID_ARTIC_3 \
#	--infolder /mnt/microbio/Nanopore/COVID_ARTIC_3/basecalled_fastq/ \
#	-profile standard

runName=$1
mkdir -p ${runName}_nanopolish
cd ${runName}_nanopolish
nextflow run ~/soft/bioinformatics_mmm_covid/main.nf \
	--run $runName \
	--infolder /mnt/microbio/Nanopore/$runName/basecalled_fastq/ \
	--f5folder /mnt/microbio/Nanopore/$runName/f5s/ \
	--seqsum /mnt/microbio/Nanopore/$runName/sequencing_summary.txt \
	--artic_repo /mnt/microbio/HOMES/nick/soft/artic-ncov2019/ \
	-profile slurm -resume
cd ../
