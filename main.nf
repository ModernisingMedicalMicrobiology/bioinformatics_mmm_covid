params.infolder=''
params.f5folder=''
params.seqsum=''
params.infolder2='not set'
params.f5folder2=''
params.seqsum2=''
params.run=''
params.artic_repo=''
params.scheme='nCoV-2019/V3'
params.meta='not set'
params.VOCs='not set'
if (params.meta != 'not set'){
Channel.from(file(params.meta))
    .splitCsv(skip: 1)
    .view { row -> "${row[2]} - ${row[1].replace("NB","barcode")} - ${row[0]}" }
    .map { cols -> tuple(cols[2], cols[1].replace("NB","barcode"), cols[0])}
    .into{ meta; meta2 }
}


if (params.infolder2 == 'not set'){
Channel
    .fromPath("$params.infolder/*.gz")
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .into{ bars }

Channel
    .fromPath("$params.f5folder/*.fast5")
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .into{ fast5s }
Channel
    .fromPath("$params.seqsum")
    .into{ seqsums }
}
else {
Channel
    .fromPath(["$params.infolder/*.gz","$params.infolder2/*.gz"])
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .into{ bars }

Channel
    .fromPath(["$params.f5folder/*.fast5","$params.f5folder2/*.fast5"])
    .map {file -> tuple(file.baseName[0..8], file) }
    .groupTuple(by: 0)
    .into{ fast5s }
Channel
    .fromPath(["$params.seqsum","$params.seqsum2"])
    .collect()
    .into{ seqsums2 }

process mergeSeqSums {
    input:
    file('*.txt') from seqsums2

    output:
    file('seqsum') into seqsums

    script:
    """
    head -n 1 1.txt > seqsum
    tail -q -n +2 *.txt >> seqsum
    """
}
}

process getVariantDefinitions {
    output:
    file('variant_definitions') into variant_definitions

    script:
    """
    git clone https://github.com/phe-genomics/variant_definitions
    """

}


process guppyplex {
    tag {barcode}

    input:
    set val(barcode), file('*.fastq.gz') from bars

    output:
    set val(barcode), file("${barcode}_.fastq") into plexed

    """
    artic guppyplex \
        --skip-quality-check \
        --min-length 400 \
        --max-length 700 \
        --directory ./ \
        --prefix ${barcode}
    """

}


process artic_minion {
    errorStrategy 'ignore'
    tag {barcode}
    cpus=2

    publishDir "pngs/", mode: 'copy', pattern: '*png'
    publishDir "consensus_seqs/", mode: 'copy', pattern: '*fasta'
    publishDir "bams/", mode: 'copy', pattern: '*bam'
    publishDir "VCFs/", mode: 'copy', pattern: '*.vcf*'

    input:                                                                      
    set val(barcode), file("${barcode}_.fastq"), file("*.fast5"), file('seqsum.txt') from plexed.combine(fast5s, by: 0).combine(seqsums)
//    file('seqsum.txt') from seqsums

    output:
    set val(barcode), file("${run}_${barcode}.sorted.bam") into (bams,bams2)
    set val(barcode), file("*.png") into pngs
    file("*.vcf*") into vcfs
    file("${run}_${barcode}.consensus.fasta") into (conSeqs, conSeqs2)
    file("${run}_${barcode}.primertrimmed.rg.sorted.bam") into primerTrimmed
    set val(run), val(barcode), file("${run}_${barcode}.consensus.fasta"), file("${run}_${barcode}.primertrimmed.rg.sorted.bam") into artminOut, artminOut2

    script:
    run=params.run
    repo=params.artic_repo
    scheme=params.scheme
    """
    source activate artic
    export LD_LIBRARY_PATH=/opt/conda/envs/artic/lib/
    artic minion \
        --normalise 200 \
        --threads ${task.cpus} \
        --scheme-directory ${repo}/primer_schemes \
        --read-file ${barcode}_.fastq \
        --fast5-directory ./ \
        --sequencing-summary seqsum.txt \
        ${scheme}  ${run}_${barcode}
    """
}

process pysamStats {                                                              
    tag { barcode }                                                                  
                                                                                    
    publishDir 'pysamStats',mode: 'copy'                                                      
                                                                                    
    input:                                                                          
    set val(barcode), file("sorted.bam") from bams2
                                                                                    
    output:                                                                         
    set val(barcode), file("${run}_${barcode}_pysam.tsv") into pStats                            
                                                                                    
    script:                                         
    run=params.run 
    repo=params.artic_repo                                                      
    scheme=params.scheme
    """ 
    samtools index sorted.bam 
    pysamstats -t variation_strand sorted.bam -f \
        ${repo}/primer_schemes/${scheme}/nCoV-2019.reference.fasta > ${run}_${barcode}_pysam.tsv                
    """                                                                             
} 

process bam_analysis {                                                              
tag { barcode }                                                                  
                                                                                
publishDir 'coverageStats', mode: 'copy'                                        
                                                                                
input:                                                                          
set val(barcode), file("sorted.bam") from bams
                                                                                
output:                                                                         
set val(barcode), file("${run}_${barcode}.csv") into analysis                            
                                                                                
script:                                         
run=params.run                                
"""                  
samtools index sorted.bam                                                           
samtools depth -aa sorted.bam > depth.tsv                   

bamStats.py depth.tsv ${run}_${barcode} None                             

"""                                                                             
} 

process pangolin {
    tag {run}

    publishDir 'pangolin', mode: 'copy'

    input:
    file('*.fasta') from conSeqs.collect()

    output:
    file("lineage_report.csv") into pangolout

    script:
    repo=params.artic_repo
    """
    cat *.fasta > unaligned.fasta
    pangolin unaligned.fasta
    """
}            

process nextclade {                                                             
    tag {run}                                                                   
                                                                                
    publishDir 'nextclade', mode: 'copy'                                         
                                                                                
    input:                                                                      
    file('*.fasta') from conSeqs2.collect()                                      
                                                                                
    output:                                                                     
    set file("${run}_tree.json"), file("${run}.tsv") into ncOut
                                                                                
    script:                                                                     
    run=params.run                                                              
    """                                                                         
    cat *.fasta > sequences.fasta                                               
    nextclade --input-fasta 'sequences.fasta' \
        --output-tree ${run}_tree.json \
        --output-tsv ${run}.tsv \
        --output-json ${run}.json                                               
    """                                                                         
}

if (params.meta != 'not set'){
process renameOutput {
	tag {run + ' ' + barcode}

	publishDir "${run}/${cog}/", mode: 'copy'

        input:
	set val(run), val(barcode), file("${run}_${barcode}.consensus.fasta"), file("${run}_${barcode}.primertrimmed.rg.sorted.bam"), val(cog) from artminOut.combine(meta, by:[0,1])

        output:
        set file("${cog}.fas"), file("${cog}.bam"), file("${cog}.md5.txt") into renamed

	script:
	run=params.run
	"""
        echo ">${cog}" > ${cog}.fas
        tail -n +2 ${run}_${barcode}.consensus.fasta >> ${cog}.fas
        mv ${run}_${barcode}.primertrimmed.rg.sorted.bam ${cog}.bam
	md5sum ${cog}* > ${cog}.md5.txt
	"""
}

process VOCs {
	tag {run + ' ' + barcode}

	publishDir 'VOCs', mode: 'copy'

	input:
	set val(run), val(barcode), file("${run}_${barcode}.consensus.fasta"), file("${run}_${barcode}.primertrimmed.rg.sorted.bam"), val(cog), file('variant_definitions') from artminOut2.combine(meta2, by:[0,1]).combine(variant_definitions)

	output:
	set val(run), file("${cog}.csv") optional true into VOC_out

	script:
	run=params.run
	repo=params.artic_repo
	scheme=params.scheme
	"""
        echo ">${cog}" > ${cog}.fas
        tail -n +2 ${run}_${barcode}.consensus.fasta >> ${cog}.fas
        cat ${repo}/primer_schemes/${scheme}/nCoV-2019.reference.fasta  ${cog}.fas > unaligned.fasta
        mafft --auto unaligned.fasta > aln.fasta
        aln2type sample_json_out \
		sample_csv_out \
		${cog}.csv \
		MN908947.3 \
		aln.fasta \
		variant_definitions/variant_yaml/*.yml
	"""

}
}    
